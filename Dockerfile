FROM ubuntu:xenial
MAINTAINER Husin Wijaya <husein@ubs-indonesia.net>

# Install essentials
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 && \
    echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y openjdk-8-jre-headless git mongodb-org mysql-server && \
    apt-get clean && \
    apt-get autoclean && \
    apt-get autoremove && \
    mkdir -p /var/lib/mysql && \
    mkdir -p /var/run/mysqld && \
    mkdir -p /var/log/mysql && \
    chown -R mysql:mysql /var/lib/mysql && \
    chown -R mysql:mysql /var/run/mysqld && \
    chown -R mysql:mysql /var/log/mysql


#RUN sed -i -e "$ a [client]\n\n[mysql]\n\n[mysqld]"  /etc/mysql/my.cnf && \
#	sed -i -e "s/\(\[client\]\)/\1\ndefault-character-set = utf8/g" /etc/mysql/my.cnf && \
#	sed -i -e "s/\(\[mysql\]\)/\1\ndefault-character-set = utf8/g" /etc/mysql/my.cnf && \
#	sed -i -e "s/\(\[mysqld\]\)/\1\ninit_connect='SET NAMES utf8'\ncharacter-set-server = utf8\ncollation-server=utf8_unicode_ci\nbind-address = 0.0.0.0/g" /etc/mysql/my.cnf

#RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
EXPOSE 22 27017 3306

#RUN mkdir -p /shms-service/build/lib/
#WORKDIR /shms-service/build/lib/
#COPY ["build/lib/shms-service-0.0.1-SNAPSHOT.jar", "."]
#WORKDIR /shms-service
#COPY ["java-server-run","."]

#RUN git clone https://nisuhw:cnn2Z8JAxvUu3UTy4e8c@bitbucket.org/nisuhw/shms-service.git
#WORKDIR /shms-service
#RUN ./gradlew build -x test

#CMD git pull
#CMD gradle clean build
#CMD mongod --config /etc/mongod.conf --fork
#CMD mysqld_safe
#CMD pa aux | grep [m]ongo
#CMD pa aux | grep [m]ysql
#CMD ["./java-server-rebuild"]
