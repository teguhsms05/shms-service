import groovy.sql.Sql

import static ratpack.groovy.Groovy.ratpack

@Grapes([
        @GrabConfig(systemClassLoader = true),
        @Grab('io.ratpack:ratpack-groovy:1.5.4'),
        @Grab('org.slf4j:slf4j-simple:1.7.25'),
        @Grab('mysql:mysql-connector-java:5.1.12'),
])

def sqlUbs = Sql.newInstance('jdbc:mysql://103.253.107.63/', 'ubs', 'ubsubs123', 'com.mysql.jdbc.Driver')
def sqlPu = Sql.newInstance('jdbc:mysql://103.211.50.254/', 'ubs', 'ubsubs123', 'com.mysql.jdbc.Driver')
def data = ['ubs': [:], 'pu': [:]]
System.addShutdownHook { sqlUbs.close() }
System.addShutdownHook { sqlPu.close() }

Thread.start {
    while (!Thread.currentThread().isInterrupted()) {
        [sqlUbs, sqlPu].each { sqlInstance ->
            ['hmi_soekarno', 'hmi_merah_putih'].each { dbName ->
                sqlInstance.eachRow("show tables from $dbName".toString()) { row ->
                    def tableName = row[0]
                    try {
                        sqlInstance.eachRow("select count(*) from ${dbName}.${tableName}".toString()) { count ->
                            if (count[0] > 0) {
                                sqlInstance.eachRow("select max(datetime) from ${dbName}.${tableName}".toString()) { datetime ->
                                    def fullTableName = "${dbName}.${tableName}".toString()
                                    def lastTime = datetime[0]
                                    data[sqlInstance == sqlUbs ? 'ubs' : 'pu'][fullTableName] = lastTime
                                    println fullTableName + " : " + lastTime
                                }
                            }
                        }
                    } catch (any) {
                    }
                    Thread.sleep(1000)
                }
            }
        }

    }
}

ratpack {
    handlers {
        get {
            render "/ubs and /pu"
        }
        get(":db") {
            response.contentType("text/html")
            def html = new StringBuilder("""<head>
<style>
table {
    border-spacing: 1px;
    border: 1px solid #ddd;
}

th {
    cursor: pointer;
}

th, td {
    text-align: left;
    padding: 3px;
}

tr:nth-child(even) {
    background-color: #f2f2f2
}
</style>
</head>""")
            html.append("<body><table id=\"myTable\" border=\"1\"><tr><th onclick=\"sortTable(0)\">Table</th><th onclick=\"sortTable(1)\">Last Data</th></tr>")
            data[pathTokens.db].each { k, v -> html.append("<tr><td>$k</td><td>$v</td></tr>") }
            html.append("</table></body>")
            html.append("""
<script>
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("myTable");
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc"; 
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++; 
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script>""")
            response.send("<!DOCTYPE html><html>" + html + "</html>")
        }
    }
}
