package com.ubs.shms.model

import groovy.transform.EqualsAndHashCode

import java.time.LocalDateTime

/**
 * Created by nisuhw on 12/15/15.
 */
@EqualsAndHashCode
class UpsDataModel {
    Double internalTemp
    String runtimeRemaining
    Double inputVoltage
    Double outputVoltage
    Double inputFrequency
    Double battCapacity
    Double battVoltage
    LocalDateTime timestamp
}
