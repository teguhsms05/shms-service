package com.ubs.shms.model

/**
 * Created by Sin on 1/10/2016.
 */
class SeismicDataEntity {
    double timeSecond
    double x0hneG
    double y0hnnG
    double z0hnzG

    SeismicDataEntity(double timeSecond, double x0hneG, double y0hnnG, double z0hnzG) {
        this.timeSecond = timeSecond
        this.x0hneG = x0hneG
        this.y0hnnG = y0hnnG
        this.z0hnzG = z0hnzG
    }
}
