package com.ubs.shms.model

import java.time.LocalDate
import java.time.LocalTime

/**
 * Created by Sin on 1/10/2016.
 */
class SeismicModel {
    String fileName
    String stationCode
    String samplingRate
    LocalDate startDate
    LocalTime startTime
    List<SeismicDataEntity> detail = []
}
