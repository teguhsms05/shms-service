package com.ubs.shms

import com.ubs.shms.util.MysqlSocketFactory
import groovy.util.logging.Slf4j
import org.jooq.exception.DataAccessException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.scheduling.config.ScheduledTaskRegistrar
import org.springframework.scheduling.support.TaskUtils

import static com.ubs.shms.util.ApplicationUtil.sleep

@Slf4j
@EnableScheduling
//@EnableCaching
@SpringBootApplication(exclude = [DataSourceAutoConfiguration, DataSourceTransactionManagerAutoConfiguration, MongoAutoConfiguration])
class ShmsServiceApplication implements SchedulingConfigurer {
    public static volatile boolean shutdown = false
    static {
        System.setProperty("org.jooq.no-logo", "true")
        //TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC))
        System.addShutdownHook {
            shutdown = true
            MysqlSocketFactory.close()
        }
    }

    static void main(String[] args) {
        def s = new ServerSocket(System.getProperty("SHUTDOWN_PORT", "55555").toInteger())
        Thread.startDaemon 'ShutdownThread', {
            s.accept()
            exit(-4)
            while (true) {
                Thread.sleep(1000)
            }
        }
        def context = SpringApplication.run ShmsServiceApplication, args
        context.registerShutdownHook()
        log.info('finish load spring context')
    }
    @Autowired
    Environment env

    @Bean
    @ConfigurationProperties("db.resensys-remote")
    DataSourceProperties resensysRemoteDb() { new DataSourceProperties() }

    @Bean
    @ConfigurationProperties("db.resensys-local")
    DataSourceProperties resensysDataTransferDb() { new DataSourceProperties() }

    @Bean
    @ConfigurationProperties("db.hmi-local.ambon")
    DataSourceProperties hmiLocalAmbonDb() { new DataSourceProperties() }

    @Bean
    @ConfigurationProperties("db.hmi-remote.ambon")
    DataSourceProperties hmiRemoteAmbonDb() { new DataSourceProperties() }

    @Bean
    @ConfigurationProperties("db.hmi-local.manado")
    DataSourceProperties hmiLocalManadoDb() { new DataSourceProperties() }

    @Bean
    @ConfigurationProperties("db.hmi-remote.manado")
    DataSourceProperties hmiRemoteManadoDb() { new DataSourceProperties() }

    @Bean
    @ConfigurationProperties("db.hmi-integration")
    DataSourceProperties hmiIntegrationDb() { new DataSourceProperties() }

    @Autowired
    @Bean(initMethod = "initialize", destroyMethod = "shutdown")
    ThreadPoolTaskScheduler scheduler() {
        new ThreadPoolTaskScheduler().with {
            setPoolSize(env.getProperty('scheduler-size', Integer))
            setWaitForTasksToCompleteOnShutdown(true)
            setAwaitTerminationSeconds(300)
            setErrorHandler({
                /*if (Throwables.getCausalChain(it).any { it instanceof IOException }) {
                    log.error("IOException", it)
                    sleep(10_000)
                } else*/
                if (it instanceof DataAccessException) {
                    log.warn("shutting down application because ", it)
                    exit(-1)
                } else if (!shutdown) {
                    TaskUtils.getDefaultErrorHandler(true).handleError(it)
                    sleep(1800_000L)
                } else {
                    log.warn("shutdown warn: " + it)
                }
            })
            it
        }
    }

    @Override
    void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setTaskScheduler(scheduler())
    }

    static void exit(int exitCode) {
        shutdown = true
        log.warn("shutting down...")
        Thread.start {
            System.exit(exitCode)
        }
    }
}
