package com.ubs.shms.service.ambon

import com.ubs.shms.service.HmiAdapterService
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/19/17.
 */
@Slf4j
@Service
@CompileStatic
@Profile('service-hmi-adapter-ambon')
class HmiAdapterServiceAmbon extends HmiAdapterService {
    @Autowired
    HmiAdapterServiceAmbon(Environment env) {
        super(env.getProperty('db.stage2.ambon'),
                env.getProperty('db.fft.ambon'),
                'ambon',
                env.getProperty("service-hmi-adapter.ambon.db-name"))
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void anmbi() {
        integrate('anmbi', ['raw_speed': '28301', 'speed': 'speed', 'direction_azimuth': '28302']) {
            it['speed'] = (it['28301'] as Double) * 3.6d
        }.with {
            if (it) log.debug("process $it anmbi data")
        }
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void anmtri() {
        integrate('anmtri', ['raw_speed': '28500', 'speed': 'speed', 'direction_azimuth': '28501', 'direction_elevation': '28502']) {
            it['speed'] = (it['28500'] as Double) * 3.6d
        }.with {
            if (it) log.debug("process $it anmtri data")
        }
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void strtr() {
        def pad = { it.toString().padLeft(7, "STRTR0") }
        (1..35).collect(pad).each {
            integrate(it, ['raw_value': '28600', 'value': 'value']) {
                it['value'] = toMpa(it['28600'] as double)
            }.with {
                if (it) log.debug("process $it strtr 1..35 data")
            }
        }
        (36..59).collect(pad).each {
            integrate(it, ['raw_value': '28601', 'value': 'value']) {
                it['value'] = toMpa(it['28601'] as double)
            }.with {
                if (it) log.debug("process $it strtr 36..59 data")
            }
        }
    }
}
