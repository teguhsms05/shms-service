package com.ubs.shms.service.ambon

import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.util.DataSourceUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import jssc.SerialPort
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

@Slf4j
@Service
@CompileStatic
@Profile('service-pln-genset-ambon')
class PlnGensetServiceAmbon {
    @Autowired
    DataSourceProperties hmiLocalAmbonDb
    @Autowired
    Environment environment
    DSLContext hmiLocal

    @PostConstruct
    void init() {
        hmiLocal = using(DataSourceUtil.init(hmiLocalAmbonDb), MYSQL)
    }

    @Scheduled(fixedDelayString = '${service-pln-genset.tempInterval}')
    void monitorArduino() {
        log.info("start ambon pln genset monitoring")
        String portName = environment.getProperty("service-pln-genset-ambon.serial_port")
        SerialPort serialPort = new SerialPort(portName)
        log.info("open port: " + portName + " -> " + serialPort.openPort())
        log.info("setting port param 115200 N-8-1 : " + serialPort.setParams(115200, 8, 1, 0))
        System.addShutdownHook {
            try {
                serialPort.closePort()
            } catch (any) {
            }
        }
        try {
            while (!ShmsServiceApplication.shutdown) {
                String str = serialPort.readString("input2: 1\r\n".getBytes().length).trim()
                if (str.empty) continue
                log.info("receive event pln genset: " + str)
                String[] response = str.split(" ")
                hmiLocal.insertInto(table('plngenset'),
                        ['datetime', 'type', 'status'].collect {
                            field(it)
                        }).values(
                        new Date(),
                        response[0].startsWith("input2") ? "PLN" : "GENSET",
                        response[1].equals("1") ? 1 : 0).onDuplicateKeyIgnore().execute()

                Thread.sleep(1100)
            }
        }
        finally {
            serialPort.closePort()
        }
    }
}
