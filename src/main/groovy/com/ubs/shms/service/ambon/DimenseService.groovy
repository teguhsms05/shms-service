package com.ubs.shms.service.ambon

import com.ubs.shms.util.Constant
import com.ubs.shms.util.DataSourceUtil
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/14/17.
 */
@Slf4j
@Service
@Profile('service-dimense')
class DimenseService {
    @Autowired
    Environment environment

    DSLContext hmiLocalDb

    @Autowired
    DimenseService(DataSourceProperties hmiLocalAmbonDb) {
        hmiLocalDb = using(DataSourceUtil.init(hmiLocalAmbonDb), MYSQL)
    }

    @Scheduled(fixedDelayString = '${service-dimense.runInterval}')
    void run() {
        (1..3).each {
            def tableName = "dsp${it.toString().padLeft(2, '0')}_join"
            Map<String, Map<Date, Double>> data = ["pt${it}.csv", "pt${it}_mean.csv"].collect {
                "http://192.168.1.127/$it".toURL().text.readLines().tail()
                        .findAll { !it.empty && !it.startsWith("0/0/") }
                        .collectEntries {
                    it.split(",").with {
                        [(Date.parse('M/d/y H:m', it[0])): it[1].toDouble().round(3)]
                    }
                }
            }.with {
                ['dynamics': it[0], 'statics': it[1]]
            }
            def lastTime = hmiLocalDb.select(max(field('datetime'))).from(tableName)
                    .fetch().get(0).get(0) as Date
            if (!lastTime)
                lastTime = Constant.SHMS_BEGINNING_OF_TIME
            new HashSet<Date>(data.dynamics.keySet() + data.statics.keySet())
                    .findAll { it.after(lastTime) && it.before(new Date()) }
                    .each {
                def val = [it, data.dynamics[it], data.statics[it]]
                hmiLocalDb.insertInto(table(tableName), ['datetime', 'dynamic', 'static']
                        .collect { field(it) }).values(val).execute()
                log.info("add to $tableName: $val")
            }
        }
    }
}
