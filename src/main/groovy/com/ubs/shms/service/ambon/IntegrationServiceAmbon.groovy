package com.ubs.shms.service.ambon

import com.ubs.shms.service.IntegrationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/6/17.
 */
@Service
@Profile('service-integration-ambon')
class IntegrationServiceAmbon extends IntegrationService {

    @Autowired
    IntegrationServiceAmbon(Environment env) {
        super(env.getProperty('db.stage1.ambon'),
                env.getProperty('db.stage2.ambon'),
                env.getProperty('db.fft.ambon'))
    }
}
