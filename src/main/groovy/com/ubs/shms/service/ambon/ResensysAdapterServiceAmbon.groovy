package com.ubs.shms.service.ambon

import com.ubs.shms.service.ResensysAdapterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/3/17.
 */
@Service
@Profile('service-resensys-adapter-ambon')
class ResensysAdapterServiceAmbon extends ResensysAdapterService {

    @Autowired
    ResensysAdapterServiceAmbon(Environment env) {
        super(env.getProperty('db.stage1.ambon'),
                env.getProperty('db.site-id.ambon').split(','))
    }
}
