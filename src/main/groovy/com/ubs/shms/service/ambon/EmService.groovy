package com.ubs.shms.service.ambon

import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.util.DataSourceUtil
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/14/17.
 */
@Slf4j
@Service
@Profile('service-em')
class EmService {
    @Autowired
    Environment environment

    DSLContext hmiLocalDb

    @Autowired
    EmService(DataSourceProperties hmiLocalAmbonDb) {
        hmiLocalDb = using(DataSourceUtil.init(hmiLocalAmbonDb), MYSQL)
    }

    @Scheduled(fixedDelayString = '${service-em.runInterval}')
    void startEm1() {
        startEm(0)
    }

    @Scheduled(fixedDelayString = '${service-em.runInterval}')
    void startEm2() {
        startEm(1)
    }

    void startEm(int emOffset) {
        String ip
        new Socket(ip = environment.getProperty("em${emOffset + 1}.ip"),
                environment.getProperty("em${emOffset + 1}.port", Integer))
                .withCloseable { Socket s ->
            System.addShutdownHook { s.close() }
            s.soTimeout = 1800_000
            log.info("connected to em ${emOffset + 1} with ip: $ip")
            def reader = s.inputStream.newReader()
            while (!ShmsServiceApplication.shutdown) {
                String line = reader.readLine()
                def split = line.split(",")
                if (split.size() == 5 + 12 * 2) {
                    split = split[5..-1]
                    12.times {
                        def emId = it + 1
                        if (emOffset) emId += 12
                        def tableName = emId.toString().padLeft(4, 'em0')
                        def rawTemp = split[it * 2] as double
                        def temp = convertToCelsius(rawTemp) as double
                        def rawValue = split[it * 2 + 1] as double
                        def value = nToMpa(tableName, rawValue) as double
                        hmiLocalDb.insertInto(table(tableName),
                                ['datetime', 'raw_temp', 'temp', 'raw_value', 'value'].collect {
                                    field(it)
                                }).values(new Date(), rawTemp, temp, rawValue, value).execute()
                        log.info("save data to $tableName ${temp.round(3)} celcius ${value.round(3)} mpa")
                    }
                } else log.trace("got input: $split from em ${emOffset + 1}")
            }
        }
    }

    double nToMpa(String table, double value) {
        value * 1000 / environment.getProperty("constant." + table.toUpperCase(), Double.class)
    }

    double convertToCelsius(double fahrenheit) {
        (fahrenheit - 32) * 5.0 / 9.0
    }

}
