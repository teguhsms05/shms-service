package com.ubs.shms.service.ambon

import com.ubs.shms.util.DataSourceUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/11/17.
 */
@Slf4j
@Service
@CompileStatic
@Profile('service-temp-monitoring')
class TempMonitoringService {

    @Autowired
    DataSourceProperties hmiLocalAmbonDb
    @Autowired
    Environment environment
    DSLContext hmiLocal

    Map<String, Double> lastTemp = [:]

    @PostConstruct
    void init() {
        hmiLocal = using(DataSourceUtil.init(hmiLocalAmbonDb), MYSQL)
    }

    @Scheduled(fixedDelayString = '${service-wind-and-temp-monitoring.tempInterval}')
    void monitorTemp() {
        (1..2).each { i ->
            def ip = environment.getProperty("pi-temp${i}.ip")
            "http://$ip:4567/temp".with {
                def response = it.toURL().readLines().first()
                String tableName = "racktemp0$i"
                Double temp = response[1..-2].split(':')[1].toDouble()
                if (temp.toInteger() != lastTemp.get(tableName)?.toInteger()) {
                    log.debug("http get $it: $response")
                    lastTemp.put(tableName, temp)
                    hmiLocal.insertInto(table(tableName), ['datetime', '`value`'].collect {
                        field(it)
                    }).values(new Date(), temp).execute()
                }
            }
        }
    }

}
