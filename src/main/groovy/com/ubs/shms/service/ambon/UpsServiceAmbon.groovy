package com.ubs.shms.service.ambon

import com.ubs.shms.service.UpsService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/12/17.
 */
@Slf4j
@Service
@Profile('service-ups-ambon')
class UpsServiceAmbon extends UpsService {

    @Autowired
    UpsServiceAmbon(DataSourceProperties hmiLocalAmbonDb, Environment environment) {
        super(hmiLocalAmbonDb, ['ups01', 'ups02'].collect {
            new Tuple2<>(environment.getProperty("service-ups-ambon.$it"), it)
        })
    }
}
