package com.ubs.shms.service.ambon

import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.util.DataSourceUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.util.concurrent.Executors

import static com.ubs.shms.util.ApplicationUtil.sleep
import static java.util.concurrent.TimeUnit.SECONDS
import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/11/17.
 */
@Slf4j
@Service
@CompileStatic
@Profile('service-wind-monitoring')
class WindMonitoringService {

    @Autowired
    DataSourceProperties hmiLocalAmbonDb
    @Autowired
    DataSourceProperties hmiRemoteAmbonDb
    @Autowired
    Environment environment
    DSLContext hmiRemote
    DSLContext hmiLocal
    double lastWindSpeed = 0
    Map<String, Double> lastTemp = [:]

    @PostConstruct
    void init() {
        hmiLocal = using(DataSourceUtil.init(hmiLocalAmbonDb), MYSQL)
        hmiRemote = using(DataSourceUtil.init(hmiRemoteAmbonDb), MYSQL)
    }

    @Scheduled(fixedDelayString = '${service-wind-and-temp-monitoring.windInterval}')
    void monitorWind() {
        Map<String, Object> map = hmiRemote.select(['id', 'speed'].collect { field(it) })
                .from(table('anmtri01'))
                .orderBy(field('datetime').desc())
                .limit(1).fetch().get(0).intoMap()
        def windSpeed = map.get('speed').toString().toDouble().intValue()
        if (windSpeed == lastWindSpeed) {
            return
        }
        lastWindSpeed = windSpeed
        def threshold = hmiLocal.select((1..2).collect { field("threshold_$it") })
                .from(table('sensor'))
                .where(field('table_name').eq('anmtri01'))
                .fetch().first().intoMap()
        [3, 2, 5].each { setWarningLight(it, 0) }
        int gpioId = windSpeed > ((double) threshold.get("threshold_2")) ? 3 : windSpeed > ((double) threshold.get("threshold_1")) ? 2 : 5
        setWarningLight(gpioId, 1)
        log.info("set gpioId $gpioId")
        Thread.start {
            "http://localhost:4567/wind/$windSpeed/$gpioId".with {
                log.debug("update wind speed=$windSpeed with id=${map.id}: " + it.toURL().readLines())
            }
        }.join(5_000L)
    }

    void setWarningLight(int gpioId, int val) {
        Executors.newFixedThreadPool(2).with { threadPool ->
            (1..2).each { i ->
                threadPool.execute({
                    def ip = environment.getProperty("pi-wl${i}.ip")
                    ("http://$ip:4567/set/$gpioId/$val").with {
                        def logMsg = "http get $it: " + it.toURL().readLines()
                        if (val == 1)
                            log.debug(logMsg)
                        else log.trace(logMsg)
                    }
                })
            }
            threadPool.shutdown()
            while (!threadPool.awaitTermination(1, SECONDS) && !ShmsServiceApplication.shutdown)
                threadPool.shutdownNow()
        }
        sleep(5000)
    }
}
