package com.ubs.shms.service.ambon

import com.ubs.shms.service.HmiDbTransferService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/2/17.
 */
@Service
@Profile('service-hmi-db-transfer-ambon')
class HmiDbTransferServiceAmbon extends HmiDbTransferService {

    @Autowired
    HmiDbTransferServiceAmbon(DataSourceProperties hmiLocalAmbonDb,
                              DataSourceProperties hmiRemoteAmbonDb,
                              Environment env) {
        super(hmiLocalAmbonDb,
                hmiRemoteAmbonDb,
                TimeZone.getTimeZone(env.getProperty("hmi-db-transfer.remoteTimezone")),
                TimeZone.getTimeZone(env.getProperty("hmi-db-transfer.localTimezoneAmbon"))
        )
    }

}
