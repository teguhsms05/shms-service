package com.ubs.shms.service.ambon

import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.service.SeismicService
import com.ubs.shms.util.DataSourceUtil
import groovy.io.FileType
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.jooq.DeleteConditionStep
import org.jooq.Record
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import java.time.ZoneId
import java.util.concurrent.atomic.AtomicInteger

import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/13/17.
 */
@Slf4j
@Service
@Profile('service-seismic-ambon')
class SeismicServiceAmbon extends SeismicService {
    @Value('${service-seismic-ambon.seismicTriggerPath}')
    File seismicTriggerPath

    @Autowired
    SeismicServiceAmbon(DataSourceProperties hmiLocalAmbonDb) {
        hmiLocal = using(DataSourceUtil.init(hmiLocalAmbonDb), MYSQL)
    }

    @Scheduled(fixedDelayString = '${service-seismic.runInterval}')
    synchronized void run() {
        seismicTriggerPath.eachFileMatch(FileType.FILES, ~/TRG.*\.txt/) {
            if (ShmsServiceApplication.shutdown) return;
            String tableName = it.name.contains("103074") ? "ssmac01" : "ssmac02";
            processHeaderDetail(tableName, it)
        }
    }

    @Scheduled(fixedDelayString = '${service-seismic.cleanUpInterval}',
            initialDelayString = '${service-seismic.cleanUpInterval}')
    synchronized void cleanUpOrphanSeismicDetail() {
        //delete from ssmac01_detail where header_id not in (select id from ssmac01);
        (1..2).each {
            cleanupHeaderDetail("ssmac0${it}_detail", "ssmac0$it")
        }
    }

}
