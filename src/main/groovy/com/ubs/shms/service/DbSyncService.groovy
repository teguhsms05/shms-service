package com.ubs.shms.service

import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.util.Constant
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.jooq.Field
import org.jooq.Record
import org.jooq.Table
import org.jooq.impl.DSL
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.util.function.Consumer

import static com.ubs.shms.util.Constant.RESENSYS_SENSOR_TYPE
import static org.jooq.impl.DSL.field
import static org.jooq.impl.DSL.table

/**
 * Created by sin on 7/7/17.
 */
@Slf4j
@Service
@Profile('service-db-sync')
class DbSyncService {
    DSLContext src = DSL.using('jdbc:mysql://127.0.0.1:3306/?useSSL=false&useCompression=true&autoReconnect=true&autoReconnectForPools=true&connectTimeout=1000', 'ubs', 'ubsubs123')
    DSLContext dst = DSL.using('jdbc:mysql://database.ubs-indonesia.net:3306/?useSSL=false&useCompression=true&autoReconnect=true&autoReconnectForPools=true&connectTimeout=1000', 'ubs', 'ubsubs123')
    @Value('${service-db-sync.queryLimit}')
    Integer queryLimit

    String[] hmiTables
    Table[] sensorsTables

    @PostConstruct
    void init() {
        System.addShutdownHook { src.close() }
        System.addShutdownHook { dst.close() }
        hmiTables = ['hmi_merah_putih', 'hmi_soekarno'].collect { db ->
            src.fetch("show tables in $db").collect { it.get(0, String).toUpperCase() }
                    .findAll { tbl -> !RESENSYS_SENSOR_TYPE.any { tbl.contains((String) it) } }
                    .collect { tbl -> db + "." + tbl.toLowerCase() }
        }.flatten()

        sensorsTables = src.fetch('show tables in sensors3').collect {
            table('sensors3.' + it.get(0, String))
        }
    }

    @Scheduled(fixedDelayString = '${service-db-sync.runInterval}')
    void syncHmi() {
        hmiTables.each { String t ->
            if (ShmsServiceApplication.shutdown) return;
            try {
                Date lastTime = dst.select().from(t).orderBy(field('datetime').desc())
                        .limit(1).fetch('datetime').with {
                    !it.empty ? it.get(0) as Date : Constant.SHMS_BEGINNING_OF_TIME
                }
                int counter = 0
                src.select().from(t).where(field('datetime').gt(lastTime)).orderBy(field('datetime').asc())
                        .limit(queryLimit).fetch().forEach({ Record it ->
                    if (ShmsServiceApplication.shutdown) return;
                    def data = it.intoMap()
                    data.remove('id')
                    counter += (dst.insertInto(table(t)).set(data.collectEntries { k, v -> [(field(k)): v] } as Map<? extends Field<?>, ?>).execute())
                } as Consumer<Record>)
                if (counter > 0)
                    log.info("upload $counter data to $t")
            } catch (any) {
                if (any.toString().contains("Unknown column 'datetime' in 'order clause'")) return
                else throw any
            }
        }
    }

    @Scheduled(fixedDelayString = '${service-db-sync.runInterval}')
    void syncResensys() {
        sensorsTables.each { Table dataTable ->
            if (ShmsServiceApplication.shutdown) return;
            if (dataTable.toString() == "sensors3.Info1") return
            Date lastTime = dst.select().from(dataTable).orderBy(field('Time').desc())
                    .limit(1).fetch('Time').with {
                !it.empty ? it.get(0) as Date : Constant.SHMS_BEGINNING_OF_TIME
            }
            int counter = 0
            src.select().from(dataTable).where(field('Time').greaterOrEqual(lastTime))
                    .orderBy(field('Time').asc()).limit(queryLimit * 100).fetch().forEach({ Record record ->
                if (ShmsServiceApplication.shutdown) return;
                counter += dst.insertInto(dataTable).set(record.intoMap()
                        .collectEntries { k, v -> [(field(k)): v] } as Map<? extends Field<?>, ?>)
                        .onDuplicateKeyIgnore().execute()
            } as Consumer<Record>)
            if (counter > 0)
                log.info("upload $counter data to $dataTable")
        }
    }

}
