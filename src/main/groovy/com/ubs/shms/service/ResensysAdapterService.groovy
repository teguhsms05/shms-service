package com.ubs.shms.service

import com.mongodb.MongoClient
import com.mongodb.MongoWriteException
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.IndexOptions
import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.util.Constant
import com.ubs.shms.util.DataSourceUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.bson.Document
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.scheduling.annotation.Scheduled

import javax.annotation.PostConstruct

import static com.mongodb.client.model.Indexes.ascending
import static com.mongodb.client.model.Sorts.descending
import static com.ubs.shms.util.Constant.*
import static java.time.ZoneOffset.UTC
import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.field
import static org.jooq.impl.DSL.using

/**
 * Created by sin on 7/2/17.
 */
@Slf4j
@CompileStatic
abstract class ResensysAdapterService {
    private static final Object lock = new Object()
    MongoClient mongoClient
    MongoDatabase stage1Db

    @Value('${service-resensys-adapter.queryLimit}')
    Integer queryLimit
    @Autowired
    DataSourceProperties resensysRemoteDb
    DSLContext create
    Map<String, Map<String, Object>> sensorInfo
    String[] siteId

    ResensysAdapterService(String stage1, String... siteId) {
        log.info("starting ResensysAdapterService for sid: ${siteId}")
        TimeZone.setDefault(TimeZone.getTimeZone(UTC))
        this.siteId = siteId
        mongoClient = DataSourceUtil.initMongoClient()
        stage1Db = mongoClient.getDatabase(stage1)
    }

    @PostConstruct
    void init() {
        create = using(DataSourceUtil.init(resensysRemoteDb), MYSQL)
        updateSensorInfo()
    }

    Closure<List<String>> getResensysTables = { String sid ->
        create.fetch("show tables in Data_$sid").collect { it.get(0, String) }
                .findAll { t ->
            ["Data.0000$sid", "Data.01010101", "Data.0F0F0F0F"].every {
                !t.startsWith(it as String)
            } && [3001, 3002, 3003].every {
                !t.endsWith(it.toString())
            } && ALL_RESENSYS_ID.any { t.contains(it) } &&
                    ALL_RESENSYS_DATA_FORMAT.any { t.endsWith('.' + it) }
        }.with { it.each { log.info("query resensys table: $it") }; it }
    }.memoize()

    @Scheduled(fixedDelay = 10000L)
    void updateSensorInfo() {
        synchronized (lock) {
            sensorInfo = create.select().from('resensys.Info').fetch().collectEntries {
                [(it.get("DID", String).replace('-', '')): it.intoMap()]
            }
        }
    }

    @Scheduled(fixedDelayString = '${service-resensys-adapter.runInterval}', initialDelay = 10_000L)
    void run() {
        synchronized (lock) {
            siteId.each { sid ->
                if (ShmsServiceApplication.shutdown) return
                log.info("check SID: $sid")
                getResensysTables.call(sid).each { resensysTable ->
                    if (ShmsServiceApplication.shutdown) return
                    def splitValue = resensysTable.split("\\.")
                    if (splitValue.length != 4) return
                    String sensorId, dataFormat
                    sensorId = splitValue[1]
                    dataFormat = splitValue[3]
                    Date lastTime = Constant.SHMS_BEGINNING_OF_TIME
                    MongoCollection<Document> collection = stage1Db.getCollection("stage1_${sensorId}_${dataFormat}")
                    if (collection.count() == 0) {
                        collection.createIndex(ascending("time", "seqNo", "optional", "value"), new IndexOptions().unique(true))
                        collection.createIndex(ascending("time"))
                        collection.createIndex(ascending("time2"))
                    } else {
                        lastTime = collection.find().sort(descending("time")).limit(1).first().time as Date
                    }
                    def count = 0
                    def limit = queryLimit
                    if (VIBRATION_DATA_FORMAT.any { resensysTable.endsWith(".$it") })
                        limit *= 5
                    create.select()
                            .from("Data_$sid.`$resensysTable`")
                            .where(field("Time").greaterOrEqual(lastTime))
                            .orderBy(field("Time"), field("SeqNo"))
                            .limit(limit)
                            .fetch()
                            .collect { it.intoMap() }.each {
                        if (ShmsServiceApplication.shutdown) return
                        def data = new Document()
                        def rawValue = data.rawValue = it['Value'] as Double
                        def time = data.time = it['Time'] as Date
                        def optional = data.optional = it['Optional'] as Integer
                        data.time2 = new Date(time.time - optional)
                        data.seqNo = it['SeqNo'] as Integer
                        data.dataFormat = dataFormat as Integer
                        data.siteId = sid
                        data.calibratedValue = rawValue
                        if (sensorInfo[sensorId])
                            for (int i : (1..10)) {
                                if (sensorInfo[sensorId]['M' + i].toString() == dataFormat) {
                                    def offset = data.offset = sensorInfo[sensorId]['DOF' + i] as Double
                                    def coeff = data.coeff = sensorInfo[sensorId]['COF' + i] as Double
                                    data.calibratedValue = ((rawValue - offset) * coeff) as Double
                                    break
                                }
                            }
                        else log.trace("not found sensor info of $sensorId")
                        try {
                            collection.insertOne(data)
                            count++
                        } catch (MongoWriteException e) {
                            if (!e.message.contains('E11000')) throw e
                        }

                    }
                    if (count > 0)
                        log.debug("insert $count data from table $resensysTable")
                }
            }
        }
    }

}
