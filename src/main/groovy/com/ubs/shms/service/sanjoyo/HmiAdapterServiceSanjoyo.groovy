package com.ubs.shms.service.sanjoyo

import com.ubs.shms.service.HmiAdapterService
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/19/17.
 */
@Slf4j
@Service
@CompileStatic
@Profile('service-hmi-adapter-sanjoyo')
class HmiAdapterServiceSanjoyo extends HmiAdapterService {
    @Autowired
    HmiAdapterServiceSanjoyo(Environment env) {
        super(env.getProperty('db.stage2.sanjoyo'),
                env.getProperty('db.fft.sanjoyo'),
                'sanjoyo',
                env.getProperty("service-hmi-adapter.sanjoyo.db-name"))
    }

}
