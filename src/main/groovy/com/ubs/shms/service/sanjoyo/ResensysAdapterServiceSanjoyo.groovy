package com.ubs.shms.service.sanjoyo

import com.ubs.shms.service.ResensysAdapterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/3/17.
 */
@Service
@Profile('service-resensys-adapter-sanjoyo')
class ResensysAdapterServiceSanjoyo extends ResensysAdapterService {

    @Autowired
    ResensysAdapterServiceSanjoyo(Environment env) {
        super(env.getProperty('db.stage1.sanjoyo'),
                env.getProperty('db.site-id.sanjoyo').split(','))
    }
}
