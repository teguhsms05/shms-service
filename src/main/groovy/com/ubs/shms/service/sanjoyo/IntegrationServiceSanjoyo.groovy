package com.ubs.shms.service.sanjoyo

import com.ubs.shms.service.IntegrationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/6/17.
 */
@Service
@Profile('service-integration-sanjoyo')
class IntegrationServiceSanjoyo extends IntegrationService {

    @Autowired
    IntegrationServiceSanjoyo(Environment env) {
        super(env.getProperty('db.stage1.sanjoyo'),
                env.getProperty('db.stage2.sanjoyo'),
                env.getProperty('db.fft.sanjoyo'))
    }
}
