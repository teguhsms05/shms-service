package com.ubs.shms.service

import com.google.common.collect.ImmutableList
import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.model.UpsDataModel
import com.ubs.shms.util.DataSourceUtil
import groovy.util.logging.Slf4j
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import org.jooq.DSLContext
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.scheduling.annotation.Scheduled

import static com.ubs.shms.ShmsServiceApplication.exit
import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/11/17.
 */
@Slf4j
abstract class UpsService {

    DSLContext hmi

    def column = ["datetime",
                  "internal_temperature",
                  "input_voltage",
                  "output_voltage",
                  "input_frequency",
                  "battery_capacity",
                  "battery_voltage",
                  "runtime_remaining",
                  "reference_id"]

    def ipAndTableUps
    Map<String, UpsDataModel> lastData = [:]

    UpsService(DataSourceProperties hmiProperties, List<Tuple2<String, String>> ipAndTableUps) {
        hmi = using(DataSourceUtil.init(hmiProperties), MYSQL)
        this.ipAndTableUps = ipAndTableUps
    }

    @Scheduled(fixedDelayString = '${service-ups.runInterval}')
    void run() {
        ipAndTableUps.each { Tuple2<String, String> it ->
            saveToHmi(lastData[it.first] = getUpsData(it.first), it.second)
        }
    }

    void saveToHmi(UpsDataModel data, String tableName) {
        if (data)
            hmi.insertInto(table(tableName), column.collect { field(it as String) })
                    .values(new Date(),
                    data.internalTemp,
                    data.inputVoltage,
                    data.outputVoltage,
                    data.inputFrequency,
                    data.battCapacity,
                    data.battVoltage,
                    data.runtimeRemaining,
                    new Date().getTime()).execute()
    }

    UpsDataModel getUpsData(String ipUps) {
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault()
            HttpClientContext context = HttpClientContext.create()
            URI uri = new URI("http://" + ipUps)
            CloseableHttpResponse response = httpClient.execute(new HttpGet(uri), context)
            String formLogin = EntityUtils.toString(response.getEntity())
            Document doc = Jsoup.parse(formLogin)
            HttpPost loginRequest = new HttpPost(uri.toString() + doc.getElementsByTag("form").get(0).attr("action"))
            loginRequest.setEntity(new UrlEncodedFormEntity(ImmutableList
                    .of(new BasicNameValuePair("login_username", "apc"),
                    new BasicNameValuePair("login_password", "apc"))))
            response = httpClient.execute(loginRequest, context)
            EntityUtils.consume(response.getEntity())
            uri = context.getRedirectLocations().get(0)
            response = httpClient.execute(new HttpGet(uri.toString() + "upstat.htm"), context)
            doc = Jsoup.parse(EntityUtils.toString(response.getEntity()))
            response = httpClient.execute(new HttpGet(uri.toString() + "logout.htm"), context)
            EntityUtils.consume(response.getEntity())
            UpsDataModel data = new UpsDataModel()
            data.setInternalTemp(new Double(doc.getElementById("langInternalTemp").parent().nextElementSibling().html().split("\\s")[0].replace("°C", "")))
            data.setRuntimeRemaining(doc.getElementById("langRuntimeRemaining").parent().nextElementSibling().html())
            data.setInputVoltage(new Double(doc.getElementById("langInputValtage").parent().nextElementSibling().html().split("\\s")[0]))
            data.setOutputVoltage(new Double(doc.getElementById("langOutputVoltage").parent().nextElementSibling().html().split("&nbsp;")[0]))
            data.setInputFrequency(new Double(doc.getElementById("langInputFreq").parent().nextElementSibling().html().split("&nbsp;")[0]))
            data.setBattCapacity(new Double(doc.getElementById("langCapacity").parent().nextElementSibling().html().split("&nbsp;")[0]))
            data.setBattVoltage(new Double(doc.getElementById("langbatteryVolt").parent().nextElementSibling().html().split("&nbsp;")[0]))
            if (data != lastData[ipUps]) {
                if (data.getBattCapacity() < 10 && data.getInputVoltage() < 10) {
                    shutdown()
                }
                return data
            }
            return null
        } catch (Exception e) {
            log.error("error ups: " + ipUps, e)
        }
        return null
    }

    void shutdown() throws InterruptedException, IOException {
        log.info("shutting down server because ups is about to die")
        new ProcessBuilder("C:\\Windows\\System32\\shutdown.exe", "-s", "-f", "-t", "120").start().waitFor()
        exit(-2)
    }
}
