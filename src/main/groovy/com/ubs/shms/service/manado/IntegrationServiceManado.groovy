package com.ubs.shms.service.manado

import com.ubs.shms.service.IntegrationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/6/17.
 */
@Service
@Profile('service-integration-manado')
class IntegrationServiceManado extends IntegrationService {

    @Autowired
    IntegrationServiceManado(Environment env) {
        super(env.getProperty('db.stage1.manado'),
                env.getProperty('db.stage2.manado'),
                env.getProperty('db.fft.manado'))
    }
}
