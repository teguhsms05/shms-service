package com.ubs.shms.service.manado

import com.ubs.shms.util.DataSourceUtil
import groovy.util.logging.Slf4j
import jssc.SerialPort
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.time.LocalDateTime
import java.util.regex.Matcher
import java.util.regex.Pattern

import static com.ubs.shms.util.ApplicationUtil.sleep
import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/7/17.
 */
@Slf4j
@Service
@Profile('service-load-cell')
class LoadCellService {
    double[] a = [53.614, 53.506, 53.570, 53.622, 53.450, 53.622, 53.558, 53.590, 53.554, 53.582, 53.647, 53.687]
    double[] b = [-53.78, -53.17, -53.25, -53.5, -52.82, -53.9, -53.44, -53.66, -53.6, -53.65, -53.74, -53.99]
    @Value('${service-load-cell.portName}')
    String portName
    @Autowired
    DataSourceProperties hmiLocalManadoDb
    DSLContext hmiDb
    SerialPort serialPort
    Map<Integer, Double> lastData = [:]

    @PostConstruct
    void init() {
        hmiDb = using(DataSourceUtil.init(hmiLocalManadoDb), MYSQL)
        serialPort = new SerialPort(portName)
        log.info("open port: " + portName + " -> " + serialPort.openPort())
        log.info("setting port param 9600 N-8-1 : " + serialPort.setParams(9600, 8, 1, 0))
    }

    double mpa(double kn){
        (kn * 1000) / 151.92
    }

    @Scheduled(fixedDelayString = '${service-load-cell.runInterval}')
    void run() {
        serialPort.writeString("#01\r") //send command #01 where 01 is the address of their module
        sleep(2000)
        String response = serialPort.readString()
        //ex response: >+025.12+020.45+012.78+018.97+003.24+015.35+008.07+014.79...
        Pattern p = Pattern.compile("[+-]\\d+\\.?\\d*")
        Matcher m = p.matcher(response.substring(1))
        LocalDateTime dateTime = LocalDateTime.now()
        def i = 1
        while (m.find() && i <= 12) {
            Double rawValue = new Double(m.group())
            if (rawValue != lastData.get(i)) {
                def tableName = i.toString().padLeft(4, 'lc0')
                def calibratedValue = a[i - 1] * rawValue + b[i - 1]
                hmiDb.insertInto(table(tableName),
                        ["datetime", "raw_value", "value", "value_mpa"].collect { field(it) })
                        .values(dateTime, rawValue, calibratedValue, mpa(calibratedValue)).execute()
                lastData.put(i, rawValue)
                log.info("insert into $tableName with data: ${calibratedValue.round(3)}")
            }
            i++
        }
    }

}
