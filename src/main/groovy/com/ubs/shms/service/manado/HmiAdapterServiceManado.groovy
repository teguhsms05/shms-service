package com.ubs.shms.service.manado

import com.ubs.shms.service.HmiAdapterService
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/19/17.
 */
@Slf4j
@Service
@CompileStatic
@Profile('service-hmi-adapter-manado')
class HmiAdapterServiceManado extends HmiAdapterService {
    @Autowired
    HmiAdapterServiceManado(Environment env) {
        super(env.getProperty('db.stage2.manado'),
                env.getProperty('db.fft.manado'),
                'manado',
                env.getProperty("service-hmi-adapter.manado.db-name"))
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void anmbi() {
        integrate('anmbi', ['speed': '28301', 'direction_azimuth': '28302']).with {
            if (it) log.debug("process $it anmbi data")
        }
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void anmtri() {
        integrate('anmtri', ['speed': '28500', 'direction_azimuth': '28501', 'direction_elevation': '28502']).with {
            if (it) log.debug("process $it anmtri data")
        }
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void strtr() {
        integrate("strtr", ['raw_value': '1', 'value': 'value']) {
            it['value'] = toMpa(it['1'] as Double)
        }.with {
            if (it) log.debug("process $it strtr data")
        }
        integrate("strtr25", ['raw_value': '28601', 'value': 'value']) {
            it['value'] = toMpa(it['28601'] as Double)
        }.with {
            if (it) log.debug("process $it strtr25 data")
        }
    }

}
