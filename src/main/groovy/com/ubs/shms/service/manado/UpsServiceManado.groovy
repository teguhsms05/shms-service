package com.ubs.shms.service.manado

import com.ubs.shms.service.UpsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/12/17.
 */
@Service
@Profile('service-ups-manado')
class UpsServiceManado extends UpsService {

    @Autowired
    UpsServiceManado(DataSourceProperties hmiLocalManadoDb, Environment environment) {
        super(hmiLocalManadoDb, ['ups'].collect {
            new Tuple2<>(environment.getProperty("service-ups-manado.$it"), it)
        })
    }
}
