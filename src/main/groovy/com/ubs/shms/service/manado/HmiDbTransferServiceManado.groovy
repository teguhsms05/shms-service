package com.ubs.shms.service.manado

import com.ubs.shms.service.HmiDbTransferService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service

/**
 * Created by sin on 7/2/17.
 */
@Service
@Profile('service-hmi-db-transfer-manado')
class HmiDbTransferServiceManado extends HmiDbTransferService {

    @Autowired
    HmiDbTransferServiceManado(DataSourceProperties hmiLocalManadoDb,
                               DataSourceProperties hmiRemoteManadoDb,
                               Environment env) {
        super(hmiLocalManadoDb,
                hmiRemoteManadoDb,
                TimeZone.getTimeZone(env.getProperty("hmi-db-transfer.remoteTimezone")),
                TimeZone.getTimeZone(env.getProperty("hmi-db-transfer.localTimezoneManado")))
    }

}
