package com.ubs.shms.service.manado

import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.service.SeismicService
import com.ubs.shms.util.DataSourceUtil
import groovy.io.FileType
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import java.time.ZoneId

import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/14/17.
 */
@Slf4j
@Service
@Profile('service-seismic-manado')
class SeismicServiceManado extends SeismicService {
    @Value('${service-seismic-manado.seismicTriggerPath}')
    File seismicTriggerPath

    @Autowired
    SeismicServiceManado(DataSourceProperties hmiLocalManadoDb) {
        hmiLocal = using(DataSourceUtil.init(hmiLocalManadoDb), MYSQL)
    }

    @Scheduled(fixedDelayString = '${service-seismic.runInterval}')
    void run() {
        seismicTriggerPath.eachFileMatch(FileType.FILES, ~/TRG.*\.txt/) {
            if (ShmsServiceApplication.shutdown) return;
            String tableName = "ssmac01"
            def data = readData(it)
            if (hmiLocal.fetchCount(table(tableName), field('reference_id').eq(data.fileName))
                    < data.detail.size()) {
                def date = Date.from(data.startDate.atTime(data.startTime).atZone(ZoneId.systemDefault()).toInstant())
                for (def d : data.detail) {
                    if (ShmsServiceApplication.shutdown) return;
                    hmiLocal.insertInto(table(tableName),
                            ['datetime', 'x', 'y', 'z', 'reference_id'].collect { field(it) })
                            .values(new Date(date.time + (d.timeSecond * 1000).toLong()),
                            d.x0hneG, d.y0hnnG, d.z0hnzG, data.fileName).onDuplicateKeyIgnore().execute()
                }
            }
        }
    }

    @Scheduled(fixedDelayString = '${service-seismic.runInterval}')
    synchronized void run2() {
        seismicTriggerPath.eachFileMatch(FileType.FILES, ~/TRG.*\.txt/) {
            if (ShmsServiceApplication.shutdown) return;
            processHeaderDetail( "ssmac01_header", it)
        }
    }

    @Scheduled(fixedDelayString = '${service-seismic.cleanUpInterval}',
            initialDelayString = '${service-seismic.cleanUpInterval}')
    synchronized void cleanUpOrphanSeismicDetail() {
        cleanupHeaderDetail("ssmac01_header_detail", "ssmac01_header")
    }
}
