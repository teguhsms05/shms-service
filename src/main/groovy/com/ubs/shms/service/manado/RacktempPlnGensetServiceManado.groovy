package com.ubs.shms.service.manado

import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.util.DataSourceUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct

import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

@Slf4j
@Service
@CompileStatic
@Profile('service-racktemp-pln-genset-manado')
class RacktempPlnGensetServiceManado {
    static final int UDP_BUFFER_SIZE = 255
    @Autowired
    DataSourceProperties hmiLocalManadoDb
    @Autowired
    Environment environment
    DSLContext hmiLocal

    @PostConstruct
    void init() {
        hmiLocal = using(DataSourceUtil.init(hmiLocalManadoDb), MYSQL)
    }

    @Scheduled(fixedDelayString = '${service-pln-genset.tempInterval}')
    void monitorRaspberryPi() {
        log.info("start manado racktemp pln genset monitoring")
        Integer port = environment.getProperty("service-racktemp-pln-genset-manado.port", Integer.class)
        new DatagramSocket(port).withCloseable { datagramSocket ->
            def closeSocket = {
                try {
                    datagramSocket.close()
                } catch (any) {
                }
            }
            System.addShutdownHook(closeSocket)
            datagramSocket.setSoTimeout(5 * 60 * 1000)
            try {
                while (!ShmsServiceApplication.shutdown) {
                    DatagramPacket packet = new DatagramPacket(new byte[UDP_BUFFER_SIZE], UDP_BUFFER_SIZE)
                    try {
                        datagramSocket.receive(packet)
                    } catch (Exception e) {
                        return
                    }
                    String[] message = new String(packet.getData()).trim().split("\\s")
                    hmiLocal.insertInto(table('racktemp'), ['datetime', 'value', 'reference_id'].collect {
                        field(it)
                    }).values(new Date(), message[1], new Date().time).onDuplicateKeyIgnore().execute()
                    hmiLocal.insertInto(table('plngenset'),
                            ['datetime', 'type', 'status', 'reference_id'].collect {
                                field(it)
                            }).values(new Date(), "PLN", message[2], new Date().time)
                            .onDuplicateKeyIgnore().execute()
                    hmiLocal.insertInto(table('plngenset'),
                            ['datetime', 'type', 'status', 'reference_id'].collect {
                                field(it)
                            }).values(new Date(), "GENSET", message[3], new Date().time)
                            .onDuplicateKeyIgnore().execute()
                }
            } finally {
                closeSocket.call()
            }
        }
    }
}
