package com.ubs.shms.service

import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.model.SeismicDataEntity
import com.ubs.shms.model.SeismicModel
import com.ubs.shms.util.ApplicationUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.jooq.DSLContext

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.concurrent.atomic.AtomicInteger

import static org.jooq.impl.DSL.field
import static org.jooq.impl.DSL.field
import static org.jooq.impl.DSL.field
import static org.jooq.impl.DSL.table
import static org.jooq.impl.DSL.table
import static org.jooq.impl.DSL.table

/**
 * Created by sin on 7/12/17.
 */
@Slf4j
@CompileStatic
abstract class SeismicService {

    private AtomicInteger idCounter = new AtomicInteger(1)
    protected DSLContext hmiLocal;

    SeismicModel readData(File seismicFile) {
        def data = new SeismicModel()
        data.fileName = seismicFile.name
        log.info("reading seismic trigger $data.fileName")
        seismicFile.withReader { reader ->
            data.setStationCode(reader.readLine().split("\\s+")[1])
            data.setSamplingRate(reader.readLine().split("\\s+")[1])
            data.setStartDate(LocalDate.parse(reader.readLine().split("\\s+")[1],
                    DateTimeFormatter.ofPattern("dd.MM.yyyy")))
            data.setStartTime(LocalTime.parse(reader.readLine().split("\\s+")[1]))
            LocalDateTime utc = LocalDateTime.of(data.getStartDate(), data.getStartTime())
            LocalDateTime localDT = ApplicationUtil.convertUTCToLocalTimeZone(utc)
            data.setStartDate(localDT.toLocalDate())
            data.setStartTime(localDT.toLocalTime())
            reader.readLine()
            String dataLine
            while ((dataLine = reader.readLine()) != null) {
                if (!dataLine.isEmpty()) {
                    data.detail << dataLine.split("\\s+")
                            .collect { String it -> it.toDouble() }
                            .with { new SeismicDataEntity(it[0], it[1], it[2], it[3]) }
                }
            }
        }
        return data
    }

    double calculateRichter(double g) {
        double r = Math.log10(g * 981);
        if (!Double.isFinite(r)) return 0;
        return 2 * r + 1.5;
    }

    void processHeaderDetail(String tableName, File triggerFile) {
        if (hmiLocal.fetchCount(table(tableName), field('filename').eq(triggerFile.name)) == 0) {
            def data = readData(triggerFile)
            double maxX = data.detail.collect { it.x0hneG }.max()
            double maxY = data.detail.collect { it.y0hnnG }.max()
            double maxZ = data.detail.collect { it.z0hnzG }.max()
            double richterX = calculateRichter(maxX)
            double richterY = calculateRichter(maxY)
            double richterZ = calculateRichter(maxZ)
            def date = Date.from(data.startDate.atTime(data.startTime).atZone(ZoneId.systemDefault()).toInstant())
            def id = new Date().time + idCounter.incrementAndGet()
            for (def d : data.detail) {
                if (ShmsServiceApplication.shutdown) return;
                hmiLocal.insertInto(table("${tableName}_detail"),
                        ['datetime', 'x', 'y', 'z', 'header_id'].collect { field(it) })
                        .values(new Date(date.time + (d.timeSecond * 1000).toLong()),
                        d.x0hneG, d.y0hnnG, d.z0hnzG, id).execute()
            }
            hmiLocal.insertInto(table(tableName),
                    ['id', 'datetime', 'filename', 'raw_x', 'x', 'raw_y', 'y', 'raw_z', 'z', 'reference_id']
                            .collect { field(it) })
                    .values(id, date, data.fileName, maxX, richterX, maxY, richterY, maxZ, richterZ, new Date().time)
                    .execute()
        }
    }

    protected void cleanupHeaderDetail(String detail, String header) {
        hmiLocal.deleteFrom(table(detail)).where(field('header_id')
                .notIn(hmiLocal.select(field('id')).from(header))).execute()
    }
}
