package com.ubs.shms.service

import com.mongodb.MongoClient
import com.mongodb.MongoWriteException
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.UpdateOptions
import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.util.DataSourceUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.bson.Document
import org.jtransforms.fft.DoubleFFT_1D
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler

import javax.annotation.PostConstruct
import java.util.function.Consumer

import static com.mongodb.client.model.Aggregates.*
import static com.mongodb.client.model.Filters.*
import static com.mongodb.client.model.Sorts.ascending
import static com.mongodb.client.model.Sorts.descending
import static com.mongodb.client.model.Updates.set
import static com.ubs.shms.util.Constant.ALL_RESENSYS_DATA_FORMAT
import static com.ubs.shms.util.Constant.ALL_RESENSYS_ID
import static com.ubs.shms.util.Constant.SHMS_BEGINNING_OF_TIME
import static com.ubs.shms.util.Constant.VIBRATION_DATA_FORMAT
import static java.time.ZoneOffset.UTC

/**
 * Created by sin on 7/3/17.
 */
@Slf4j
@CompileStatic
abstract class IntegrationService {
    MongoClient mongoClient = DataSourceUtil.initMongoClient()
    MongoDatabase stage1Db
    MongoDatabase stage2Db
    MongoDatabase fftDb
    @Value('${service-integration.queryLimit}')
    Integer limitQuery
    @Autowired
    ThreadPoolTaskScheduler scheduler
    @Autowired
    Environment environment

    IntegrationService(String stage1, String stage2, String fft) {
        TimeZone.setDefault(TimeZone.getTimeZone(UTC))
        stage1Db = mongoClient.getDatabase(stage1)
        stage2Db = mongoClient.getDatabase(stage2)
        fftDb = mongoClient.getDatabase(fft)
    }

    @PostConstruct
    void init() {
        stage1Db.listCollectionNames().findAll {
            String c ->
                c.startsWith('stage1_') &&
                        ALL_RESENSYS_ID.any { c.contains(it) } &&
                        ALL_RESENSYS_DATA_FORMAT.any { c.endsWith('_' + it) }
        }.groupBy { it.toString().split("_")[2] }.each { k, v ->
            log.info("init scheduler for dataformat $k : $v")
            scheduler.scheduleWithFixedDelay({
                v.each { run(it as String) }
            }, environment.getProperty('service-integration.runInterval', Long))
        }
    }

    void run(String colName) {
        if (ShmsServiceApplication.shutdown) return;
        String deviceId = null
        Integer dataFormat = null
        colName.split("_").with {
            deviceId = it[1]
            dataFormat = it[2].toInteger()
        }
        Date lastTime = getLastInsertTime(deviceId, dataFormat, stage2Db)
        Date _lastTime = lastTime
        MongoCollection<Document> stage2 = stage2Db.getCollection("stage2_$deviceId")
        if (stage2.count() == 0) {
            stage2.createIndex(ascending('time'), new IndexOptions().unique(true))
        }
        def fieldTime = VIBRATION_DATA_FORMAT.contains(dataFormat) ? 'time2' : 'time'
        Date maxTime = stage1Db.getCollection(colName)
                .aggregate([group('$time'),
                            limit(5),
                            sort(descending('time'))])
                .with { it.size() > 0 ? it.last()._id : lastTime + 30 } as Date
        int count = 0;
        stage1Db.getCollection(colName)
                //.find(and(gt(fieldTime, lastTime), lt('time', maxTime)))
                .find(gt(fieldTime, lastTime))
                .sort(ascending(fieldTime))
                .limit(limitQuery)
                .forEach({ Document doc ->
            if (ShmsServiceApplication.shutdown) return;
            lastTime = doc[fieldTime] as Date
            def insertOrUpdate = {
                stage2.updateOne(eq('time', lastTime),
                        set(dataFormat.toString(), doc['calibratedValue']),
                        new UpdateOptions().upsert(true))
            }
            try {
                insertOrUpdate.call()
            } catch (MongoWriteException e) {
                if (e.error.code == 11000) {
                    log.error("retry insert or update because: " + e)
                    insertOrUpdate.call()
                } else
                    log.error("error when insert or update because: ", e)
            }
            count++
        } as Consumer<Document>)
        setLastInsertTime(deviceId, dataFormat, lastTime, stage2Db)
        if (lastTime != _lastTime)
            log.debug("update use ${stage2Db.name};{deviceId:'$deviceId',dataFormat:$dataFormat} : $lastTime($count)")
    }

    static Date getLastInsertTime(String deviceId, int dataFormat, MongoDatabase db) {
        MongoCollection<Document> history = db.getCollection("history")
        Date lastTime = SHMS_BEGINNING_OF_TIME
        if (history.count() == 0) {
            history.createIndex(ascending('deviceId', 'dataFormat'),
                    new IndexOptions().unique(true))
        } else {
            try {
                lastTime = history.find(and(
                        eq('deviceId', deviceId),
                        eq('dataFormat', dataFormat),
                )).first().lastTime as Date
            } catch (Exception e) {
                log.trace("no last time for deviceId $deviceId and dataFormat $dataFormat")
            }
        }
        return lastTime
    }

    private void setLastInsertTime(String deviceId, int dataFormat, Date lastTime, MongoDatabase db) {
        MongoCollection<Document> history = db.getCollection("history")
        history.updateOne(and(
                eq('deviceId', deviceId),
                eq('dataFormat', dataFormat)),
                set('lastTime', lastTime), new UpdateOptions().upsert(true))
    }

    @Scheduled(fixedDelayString = '${service-integration.runInterval}')
    void runFFT() {
        stage1Db.listCollectionNames().findAll { String colName ->
            VIBRATION_DATA_FORMAT.any { colName.endsWith(it.toString()) } &&
                    colName.startsWith('stage1_')
        }.forEach({ String colName ->
            if (ShmsServiceApplication.shutdown) return;
            Date lastTime = SHMS_BEGINNING_OF_TIME
            def fftCol = fftDb.getCollection(colName.replace("stage1", "fft"))
            if (!fftCol.count()) {
                fftCol.createIndex(ascending('last'), new IndexOptions().unique(true))
                fftCol.createIndex(ascending('time'))
            } else {
                lastTime = fftCol.find().sort(descending('last')).limit(1).first().last as Date
            }
            List<Document> vibrationData = []
            def dataIterator = stage1Db.getCollection(colName).find(gt('time2', lastTime))
                    .limit(limitQuery).sort(ascending('time2')).iterator()
            while (dataIterator.hasNext()) {
                def current = dataIterator.next()
                if (vibrationData.empty || vibrationData.last().time2 == new Date((current.time2 as Date).time - 20)) {
                    vibrationData << current
                } else break
            }
            if (vibrationData.empty) {
                log.trace("no data for $colName")
                return
            }
            def fft = new Document()
            fft.time = vibrationData.first().time as Date
            fft.first = vibrationData.first().time2 as Date
            fft.last = vibrationData.last().time2 as Date
            computeFFT(vibrationData.collect { it.calibratedValue as double } as double[]).with {
                fft.highestNaturalFreq = it.first
                fft.fftData = it.second.collect {
                    def d = new Document()
                    d.x = it.first
                    d.y = it.second
                    return d
                }
            }
            fft.vibrationData = vibrationData

            fftCol.insertOne(fft)

        } as Consumer<String>)
    }

    static Tuple2<Double, List<Tuple2<Double, Double>>> computeFFT(double[] input) {
        if (input.length < 8) return new Tuple2<>(0.0d, [])
        input = (input.sum() / input.size()).with { avg ->
            input.collect { double it -> it - avg }
        }
        double[] fft = new double[input.size() * 2]
        DoubleFFT_1D fftFunction = new DoubleFFT_1D(input.size())
        def hanningWindow = { int n, int N -> 0.5d * (1 - Math.cos(2 * Math.PI * n / (N - 1))) }
        for (int i = 0; i < input.size(); i++) {
            fft[i * 2] = input[i] * hanningWindow(i, input.size()) as Double
            fft[i * 2 + 1] = 0
        }
        fftFunction.complexForward(fft)
        for (int i = 0; i < input.size(); i++) {
            input[i] = Math.sqrt(fft[i * 2] * fft[i * 2] + fft[i * 2 + 1] * fft[i * 2 + 1])
        }
        List<Tuple2<Double, Double>> xy = new LinkedList<>()
        for (int i = 0; i < input.size() / 2.0d; i++) {
            xy.add(new Tuple2<Double, Double>((i * 50 / input.size()).doubleValue(), input[i]))
        }
        double highestNaturalFreq = (xy.sort { it.second }.reverse()
                .find { it.first > 1 && it.first < 3 })?.first ?: 0.0d
        return new Tuple2<Double, List<Tuple2<Double, Double>>>(highestNaturalFreq, xy)
    }

    /*@Scheduled(fixedDelayString = '${service-integration.runInterval}')
    void mergeFFT() {
        fftDb.listCollectionNames().findAll {
            String it -> it.startsWith('fft_')
        }.forEach({ String colName ->
            String deviceId = null
            Integer dataFormat = null
            colName.split("_").with {
                deviceId = it[1]
                dataFormat = it[2].toInteger()
            }
            Date lastTime = getLastInsertTime(deviceId, dataFormat, fftDb)
            MongoCollection<Document> fftMerge = fftDb.getCollection("merge_$deviceId")
            if (fftMerge.count() == 0) {
                fftMerge.createIndex(ascending('time'), new IndexOptions().unique(true))
            }
            fftDb.getCollection(colName)
                    .find(gt('time', lastTime))
                    .sort(ascending('time'))
                    .limit(limitQuery)
                    .forEach({ Document doc ->
                if (ShmsServiceApplication.shutdown) return;
                lastTime = doc['time'] as Date
                fftMerge.updateOne(eq('time', lastTime),
                        set(dataFormat.toString(), doc['highestNaturalFreq']),
                        new UpdateOptions().upsert(true))
            } as Consumer<Document>)
            setLastInsertTime(deviceId, dataFormat, lastTime, fftDb)
        } as Consumer<String>)
    }*/
}
