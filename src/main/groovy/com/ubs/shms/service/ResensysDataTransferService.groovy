package com.ubs.shms.service

import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.util.DataSourceUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.jooq.Record
import org.jooq.UpdateSetMoreStep
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

import javax.annotation.PostConstruct
import java.util.function.Consumer

import static com.ubs.shms.util.Constant.SHMS_BEGINNING_OF_TIME
import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/6/17.
 */
@Slf4j
@Service
@CompileStatic
@Profile('service-resensys-data-transfer')
class ResensysDataTransferService {
    @Autowired
    DataSourceProperties resensysDataTransferDb
    @Autowired
    DataSourceProperties resensysRemoteDb
    DSLContext src
    DSLContext dst

    @Value('${service-resensys-data-transfer.queryLimit}')
    Integer queryLimit
    @Value('${service-resensys-data-transfer.siteIds}')
    String siteIds

    String[] columnList = ['SiteID', 'DeviceID', 'SeqNo', 'Time', 'DataFormat', 'Value', 'Optional']

    @PostConstruct
    void init() {
        src = using(DataSourceUtil.init(resensysRemoteDb), MYSQL)
        dst = using(DataSourceUtil.init(resensysDataTransferDb), MYSQL)
    }

    @Scheduled(fixedDelayString = '${service-resensys-data-transfer.runInterval}', initialDelay = 10_000L)
    synchronized void queryData() {
        siteIds.split(",").each { String sid ->
            (1..10).any {
                Date lastTime = (dst.select(field('time'))
                        .from(sid)
                        .orderBy(field('time').desc())
                        .limit(1)
                        .fetch(0).with { it.empty ? SHMS_BEGINNING_OF_TIME : it.first() }) as Date
                int count = 0
                src.select()
                        .from("sensors.$sid")
                        .where(field('time').greaterOrEqual(lastTime))
                        .orderBy(field('time'))
                        .limit(queryLimit).fetch().forEach({ Record record ->
                    if (ShmsServiceApplication.shutdown) return;
                    count += dst.insertInto(table(sid), columnList.collect { field("`$it`") })
                            .values(columnList.collect { record.get(it.toString()) })
                            .onDuplicateKeyIgnore().execute()
                } as Consumer<Record>)
                if (count > 0)
                    log.info("transfer $count data $sid where time >= $lastTime")
                else true
            }
        }
    }

    @Scheduled(fixedDelay = 3600_000L)
    synchronized void updateInfo() {
        int infoRow = 0
        src.select().from('resensys.Info').fetch().forEach({ Record record ->
            if (ShmsServiceApplication.shutdown) return;
            dst.update(table('Info1')).with { info ->
                UpdateSetMoreStep ex = null
                def row = record.intoMap()
                row.each {
                    ex = info.set(field(it.key), it.value)
                }
                if (ex?.where(field('idInfo').eq(row.get('idInfo')))?.execute()) {
                    infoRow++
                } else {
                    if (dst.insertInto(table('Info1'), row.keySet().collect { field(it) })
                            .values(row.values()).execute()) {
                        log.error("cannot update info: $row")
                    } else infoRow++
                }
            }
        } as Consumer<Record>)
        log.debug("update info $infoRow rows")
    }
}
