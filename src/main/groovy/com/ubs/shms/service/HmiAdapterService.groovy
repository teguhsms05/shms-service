package com.ubs.shms.service

import com.google.common.io.Resources
import com.mongodb.MongoClient
import com.mongodb.client.MongoDatabase
import com.ubs.shms.ShmsServiceApplication
import com.ubs.shms.util.DataSourceUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.bson.Document
import org.jooq.DSLContext
import org.jooq.impl.DSL
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.scheduling.annotation.Scheduled

import javax.annotation.PostConstruct
import java.nio.charset.Charset

import static com.mongodb.client.model.Filters.*
import static com.mongodb.client.model.Sorts.ascending
import static com.ubs.shms.util.Constant.SHMS_BEGINNING_OF_TIME
import static com.ubs.shms.util.DataSourceUtil.init
import static org.jooq.SQLDialect.MYSQL
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/6/17.
 */
@Slf4j
@CompileStatic
abstract class HmiAdapterService {
    final Map<String, String> sensorMapping
    MongoDatabase stage2Db
    MongoDatabase fftDb
    @Autowired
    DataSourceProperties hmiIntegrationDb
    DSLContext hmi
    @Value('${service-hmi-adapter.queryLimit}')
    Integer queryLimit
    Map<String, String> defaultHeader = ['reference_id': '_idStr', 'datetime': 'time']

    Closure<Map<String, String>> filterSensor = { String prefix ->
        sensorMapping.findAll {
            it.key.toLowerCase().split("\\.")[1].startsWith(prefix.toLowerCase())
        }
    }.memoize()

    Closure<Document> defaultTransformer = { Closure<?> additionalTransformer, Document it ->
        it['_idStr'] = it['_id'].toString()
        additionalTransformer(it)
        return it
    }

    HmiAdapterService(String stage2, String fft, String location, String hmiDbName) {
        MongoClient mongo = DataSourceUtil.initMongoClient()
        stage2Db = mongo.getDatabase(stage2)
        fftDb = mongo.getDatabase(fft)
        sensorMapping = Collections.unmodifiableMap(Resources.readLines(Resources.getResource("/resensys-data-mapping-${location}.csv"),
                Charset.defaultCharset()).collectEntries {
            it.split(",").with { [(hmiDbName + '.' + it[0]): it[1].replace("-", "")] }
        })
    }

    @PostConstruct
    void init() {
        hmi = using(init(hmiIntegrationDb), MYSQL)
    }

    Iterable<Document> findStage2Data(String resensysId, String hmiTable, Date upperTime) {
        String timeField = 'time'
        if (hmiTable.toUpperCase().startsWith("ACC"))
            timeField = 'time2'
        stage2Db.getCollection("stage2_$resensysId")
                .find(and(gt(timeField,
                hmi.select(max(field("datetime"))).from(hmiTable).fetch(0).get(0) ?: SHMS_BEGINNING_OF_TIME),
                lt(timeField, upperTime)))
                .sort(ascending(timeField))
                .limit(queryLimit)
    }

    Date findUpperLimitTime(String resensysId, Collection<String> dataFormats) {
        dataFormats.findAll { it.isInteger() }.collect { String it ->
            IntegrationService.getLastInsertTime(resensysId, it.toInteger(), stage2Db)
        }.min()
    }

    int integrate(String hmiPrefixTable, Map<String, String> columnMap,
                  Closure<?> transform = {}) {
        def mapping = defaultHeader + columnMap
        int count = 0
        filterSensor.call(hmiPrefixTable).each { String hmiTable, String resensysId ->
            if (ShmsServiceApplication.shutdown) return
            def dataFormats = columnMap.values()
            def limitTime = findUpperLimitTime(resensysId, dataFormats)
            def data = findStage2Data(resensysId, hmiTable, limitTime)
            data.groupBy { doc ->
                dataFormats.findAll { it.isInteger() }.every {
                    if (hmiTable.split("\\.")[1].toUpperCase().startsWith("ACCS")) {
                        if (!doc.containsKey(it)) doc[it] = null
                        return true
                    } else
                        doc.containsKey(it)
                }
            }.with {
                it[false]?.each {
                    if (ShmsServiceApplication.shutdown) return
                    stage2Db.getCollection("stage2_$resensysId").deleteOne(eq('_id', it['_id']))
                }
                it[true] ?: []
            }.collect(defaultTransformer.curry(transform)).each { doc ->
                if (ShmsServiceApplication.shutdown) return
                def param = mapping.values().collect { doc[it] }
                try {
                    count += hmi.insertInto(table(hmiTable), mapping.keySet().collect { field(it) })
                            .values(param).execute()
                } catch (any) {
                    log.error("failed executing query with param: ${param}")
                    throw any
                }
            }
            if (count > 0)
                log.trace("integrate $count data of $hmiTable ($resensysId): $columnMap")
        }
        return count
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void accVibration() {
        integrate('acc', ['x': '28000', 'y': '28100', 'z': '28200'])
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void accsVibration() {
        integrate('accs', ['x': '30000', 'y': '30100', 'z': '30200'])
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void atrh() {
        integrate('atrh', ['raw_humidity': '6001', 'humidity': 'humid_filter', 'temperature': '6002']) {
            try {
                it['humid_filter'] = Math.max(0.0d, Math.min(100.0d, it['6001'] as double))
            } catch (e) {
                log.error("missing data 6001 on " + it + " with db " + stage2Db.name, e)
                it['humid_filter'] = 0.0
            }
        }
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void temp() {
        integrate('temp', ['temperature': '6002'])
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void tilt() {
        integrate('tilt', ['x': '2001', 'y': '2002', 'z': '2003', 'hires': '7'])
    }

    static double toMpa(double strain) {
        strain / 10e6 * 4700 * Math.sqrt(40)
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void accFftXYZ() {
        filterSensor('ACC').each { String sensorId, String resensysId ->
            [['28000','28100','28200'],['30000','30100','30200']].each{List df ->
            ['x': df[0], 'y': df[1], 'z': df[2]].each { String axis, Object dataFormat ->
                if (ShmsServiceApplication.shutdown) return
                def hmiTable = "${sensorId}_fft_$axis"
                Date lastTime = hmi.select(max(field('datetime')))
                        .from(table(hmiTable))
                        .fetch(0).get(0) as Date ?: SHMS_BEGINNING_OF_TIME
                def fftCol = fftDb.getCollection("fft_${resensysId}_$dataFormat")
                int count = 0
                fftCol.find(gt('time', lastTime))
                        .sort(ascending('time'))
                        .limit(queryLimit).findAll {
                    if (it['highestNaturalFreq'] == 0) {
                        fftCol.deleteOne(eq('_id', it['_id']))
                        false
                    } else true
                }.groupBy { it['time'] }.collectEntries {
                    [(it.key): it.value.with {
                        (it.sum { it['highestNaturalFreq'] } as BigDecimal) / it.size()
                    }]
                }.each { datetime, value ->
                    if (ShmsServiceApplication.shutdown) return
                    count += hmi.insertInto(table(hmiTable), ['datetime', 'value'].collect(DSL.&field))
                            .values(datetime, value).execute()
                }
                if (count > 0)
                    log.debug("insert $count data to $hmiTable")
            }}
        }
    }

    @Scheduled(fixedDelayString = '${service-hmi-adapter.runInterval}')
    void accFft() {
        filterSensor('ACC').each { String sensorId, String resensysId ->
            if (ShmsServiceApplication.shutdown) return
            def hmiTable = "${sensorId}_fft"
            Date lastTime = hmi.select(max(field('datetime')))
                    .from(table(hmiTable))
                    .fetch(0).get(0) as Date ?: SHMS_BEGINNING_OF_TIME
            def xyz = ['x', 'y', 'z']
            Date maxTime = xyz.collect {
                hmi.select(max(field('datetime')))
                        .from(table(hmiTable + "_$it"))
                        .fetch(0).get(0) as Date ?: SHMS_BEGINNING_OF_TIME
            }.min()
            def datetimes = new LinkedHashSet<Date>()
            Map<String, Map> data = xyz.collectEntries {
                if (ShmsServiceApplication.shutdown) return [:]
                Map<Date, Double> data = hmi.select(['datetime', 'value'].collect { field(it) })
                        .from(table(hmiTable + "_$it"))
                        .where(field('datetime').gt(lastTime) & field('datetime').lt(maxTime))
                        .fetch()
                        .collectEntries { [(it.get(0, Date)): it.get(1, Double)] }
                datetimes.addAll(data.keySet())
                [(it): data]
            }
            if (ShmsServiceApplication.shutdown) return
            int count = 0
            datetimes.sort().each { Date dt ->
                if (ShmsServiceApplication.shutdown) return
                count += hmi.insertInto(table(hmiTable), ['datetime', *xyz].collect(DSL.&field))
                        .values([dt, *(xyz.collect { data[it][dt] ?: 0 })])
                        .execute()
            }
            if (count > 0)
                log.debug("insert $count data to $hmiTable")
        }
    }
}
