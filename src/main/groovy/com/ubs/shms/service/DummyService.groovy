package com.ubs.shms.service

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service

@Slf4j
@CompileStatic
@Service
@Profile('server-dummy')
class DummyService {

    @Scheduled(fixedDelay = 1L)
    void run() {
        log.info('start nothing')
        Thread.sleep(5000)
        log.info('finish nothing')
    }
}
