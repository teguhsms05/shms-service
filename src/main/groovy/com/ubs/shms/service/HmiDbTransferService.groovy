package com.ubs.shms.service

import com.google.common.collect.Iterators
import com.ubs.shms.util.ApplicationUtil
import com.ubs.shms.util.DataSourceUtil
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.scheduling.annotation.Scheduled

import static com.ubs.shms.util.Constant.RESENSYS_SENSOR_TYPE
import static com.ubs.shms.util.Constant.SHMS_BEGINNING_OF_TIME
import static org.jooq.impl.DSL.*

/**
 * Created by sin on 7/2/17.
 */
@Slf4j
@CompileStatic
abstract class HmiDbTransferService {

    DSLContext localHmi
    DSLContext remoteHmi
    Iterator<String> nonResensysTable
    Iterator<String> resensysTable
    Iterator<String> hmiSensorTable
    String localDbName
    String remoteDbName
    @Value('${hmi-db-transfer.queryLimit}')
    Integer queryLimit
    TimeZone hmiLocalTimeZone
    TimeZone hmiRemoteTimeZone
    DSLContext pu
    private static final String EXCEPTION_MSG = "Unknown column 'datetime' in 'field list'"

    HmiDbTransferService(DataSourceProperties local, DataSourceProperties remote, TimeZone remoteTz, TimeZone localTz) {
        log.info("local timezone: ${localTz.toZoneId()}")
        log.info("remote timezone: ${remoteTz.toZoneId()}")
        this.hmiLocalTimeZone = localTz
        this.hmiRemoteTimeZone = remoteTz
        this.localDbName = local.name
        this.remoteDbName = remote.name
        localHmi = using(DataSourceUtil.init(local), SQLDialect.MYSQL)
        remoteHmi = using(DataSourceUtil.init(remote), SQLDialect.MYSQL)

        Map<Boolean, List<String>> tableList = localHmi
                .fetch("show tables in $localDbName")
                .collect { it.get(0, String) }
                .findAll { t -> ['migrations', 'sessions'].every { t != it } }
                .groupBy { t ->
            RESENSYS_SENSOR_TYPE.any {
                t.toUpperCase().contains(it.toString())
            }
        }
        nonResensysTable = Iterators.cycle(tableList[false])
        resensysTable = Iterators.cycle(tableList[true])
        hmiSensorTable = Iterators.cycle(new LinkedList<String>(tableList[false] + tableList[true]))
    }

    @Scheduled(fixedDelay = 3600_000L)
    void checkConnectionMirrorDb() {
        try {
            if (pu == null)
                pu = DSL.using("jdbc:mysql://103.211.50.254:3306/$remoteDbName?rewriteBatchedStatements=true&useSSL=false&useCompression=true&autoReconnect=true&autoReconnectForPools=true&connectTimeout=600000&socketFactory=com.ubs.shms.util.MysqlSocketFactory", 'ubs', 'ubsubs123')
        } catch (any) {
            log.warn("DB PU: " + any.getMessage())
        }
    }

    @Scheduled(fixedDelayString = '${hmi-db-transfer.delay}', initialDelayString = '${hmi-db-transfer.delay}')
    void uploadAllToMirrorDb() {
        def tableName = hmiSensorTable.next()
        try {
            transfer(localHmi, pu, tableName, hmiLocalTimeZone, hmiRemoteTimeZone)
        } catch (any) {
            log.warn("DB PU: " + any.getMessage())
            try {
                pu.close()
            } catch (all) {
                log.warn("DB PU: " + all.getMessage())
            }
            pu = null
        }
    }

    @Scheduled(fixedDelayString = '${hmi-db-transfer.delay}', initialDelayString = '${hmi-db-transfer.delay}')
    void upload() {
        def tableName = nonResensysTable.next()
        transfer(localHmi, remoteHmi, tableName, hmiLocalTimeZone, hmiRemoteTimeZone)
    }

    @Scheduled(fixedDelayString = '${hmi-db-transfer.delay}', initialDelayString = '${hmi-db-transfer.delay}')
    void download() {
        def tableName = resensysTable.next()
        transfer(remoteHmi, localHmi, tableName, hmiRemoteTimeZone, hmiLocalTimeZone)
    }

    void transfer(DSLContext src, DSLContext dst, String tableName, TimeZone srcTz, TimeZone dstTz) {
        if (src == null || dst == null) return
        def sourceData = []
        try {
            try {
                if (tableName.toLowerCase().startsWith("ssmac")) throw new Exception(EXCEPTION_MSG)
                def lastDatetime = dst.select(max(field('datetime'))).from(tableName).fetch(0, Date).get(0) ?: SHMS_BEGINNING_OF_TIME
                lastDatetime = ApplicationUtil.convertTimeZone(lastDatetime as Date, dstTz, srcTz)
                sourceData = src.select().from(tableName)
                        .where(field('datetime').gt(lastDatetime))
                        .orderBy(field('datetime'))
                        .limit(queryLimit)
                        .fetch().collect {
                    it.intoMap().with { it.remove("id"); it } as Map<String, Object>
                }
            } catch (Exception e) {
                if (e.message.contains(EXCEPTION_MSG)) {
                    def lastId = dst.select(max(field('id'))).from(tableName).fetch(0, Long).get(0) ?: 0L
                    sourceData = src.select().from(tableName)
                            .where(field('id').gt(lastId))
                            .orderBy(field('id'))
                            .limit(queryLimit)
                            .fetch().collect { it.intoMap() }
                } else throw e
            }
            if (!sourceData.empty) {
                sourceData.findAll { it.containsKey('datetime') }.each { Map<String, Object> row ->
                    row.datetime = ApplicationUtil.convertTimeZone(row.datetime as Date, srcTz, dstTz)
                }
                def insertStatement = dst.insertInto(table(tableName),
                        sourceData.get(0).keySet().collect { field("`$it`") })
                sourceData.each {
                    insertStatement = insertStatement.values(it.values())
                }
                int count = insertStatement.onDuplicateKeyIgnore().execute()
                if (count)
                    log.debug("sync $count data $tableName ${dst == pu ? "PU" : "UBS"}")
            }
        } catch (any) {
            if (any.toString().contains("doesn't exist"))
                log.warn(any.toString())
            else throw any
        }
    }
}
