package com.ubs.shms.util

import com.mysql.jdbc.StandardSocketFactory
import com.ubs.shms.ShmsServiceApplication
import groovy.util.logging.Slf4j

@Slf4j
class MysqlSocketFactory extends StandardSocketFactory {
    private static Set<Socket> socketSet = new HashSet<>();

    @Override
    Socket connect(String hostname, int portNumber, Properties props) throws SocketException, IOException {
        Socket socket = null
        if (!ShmsServiceApplication.shutdown) {
            socket = super.connect(hostname, portNumber, props)
            socketSet.add(socket)
        }
        return socket
    }

    static void close() {
        socketSet.each {
            try {
                it.close()
            } catch (any) {
                log.warn(any.toString())
            }
        }
    }
}
