package com.ubs.shms.util

import com.mongodb.MongoClient
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.jdbc.datasource.DataSourceUtils

import javax.sql.DataSource

/**
 * Created by sin on 7/16/17.
 */
@Slf4j
@CompileStatic
class DataSourceUtil {
    private static Map<DataSourceProperties, DataSource> cache = [:]

    synchronized static DataSource init(DataSourceProperties dsp) {
        DataSource ret;
        try {
            return ret = cache.get(dsp) ?: cache.put(dsp, dsp.initializeDataSourceBuilder().build()) ?: cache.get(dsp)
        } finally {
            System.addShutdownHook { close(ret) }
            return ret
        }
    }

    static void close(DataSource ds) {
        try {
            if(ds instanceof org.apache.tomcat.jdbc.pool.DataSource)
                ds.close(true)
            else
                DataSourceUtils.getConnection(ds).close()
        } catch (Exception e) {
            log.warn("error closing datasource connection: " + e.toString())
        }
    }
    private static MongoClient mongoClient

    static synchronized MongoClient initMongoClient() {
        if (mongoClient == null) {
            mongoClient = new MongoClient()
            System.addShutdownHook { mongoClient.close() }
        }
        return mongoClient
    }
}
