package com.ubs.shms.util

import com.google.common.io.Resources

import java.nio.charset.Charset

import static java.time.LocalDate.of
import static java.time.LocalTime.MIN
import static java.time.Month.DECEMBER
import static java.time.ZoneOffset.UTC
import static java.util.Date.from

/**
 * Created by sin on 7/2/17.
 */
interface Constant {
    String[] RESENSYS_SENSOR_TYPE = ['ACC', 'ACCS', 'ANMBI', 'ANMTRI', 'ATRH', 'STRTR', 'TEMP', 'TILT']
    Date SHMS_BEGINNING_OF_TIME = from(of(2015, DECEMBER, 1).atTime(MIN).atZone(UTC).toInstant())
    static List<String> ALL_RESENSYS_ID = ['ambon', 'manado', 'sanjoyo'].collect { String location ->
        Resources.readLines(Resources.getResource("/resensys-data-mapping-${location}.csv"),
                Charset.defaultCharset()).collect { it.split(",")[1].replace("-", "") }
    }.flatten() as List<String>
    static List<String> ALL_RESENSYS_DATA_FORMAT = [
            1,
            7,
            2001, 2002, 2003,
            6001, 6002,
            28000, 28100, 28200,
            28301, 28302,
            28500, 28501, 28502,
            28600, 28601,
            30000, 30100, 30200
    ].collect { it.toString() }
    int[] VIBRATION_DATA_FORMAT = [28000, 28100, 28200, 30000, 30100, 30200]
}