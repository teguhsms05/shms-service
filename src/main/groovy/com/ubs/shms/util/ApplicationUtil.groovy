package com.ubs.shms.util

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset

/**
 * Created by sin on 7/12/17.
 */
class ApplicationUtil {

    static LocalDateTime convertUTCToLocalTimeZone(LocalDateTime utc) {
        return utc.atOffset(ZoneOffset.UTC).atZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
    }

    static synchronized void sleep(long milis) {
        def currentThread = Thread.currentThread()
        def shutdownHook = new Thread({
            currentThread.interrupt()
        });
        Runtime.runtime.addShutdownHook(shutdownHook)
        Thread.sleep(milis)
        Runtime.runtime.removeShutdownHook(shutdownHook)
    }

    /*static Date convertLocalTimeZone(Date d, TimeZone target) {
        Date.parse('yyyy-MM-dd HH:mm:ss.SSS', d.format('yyyy-MM-dd HH:mm:ss.SSS', target))
    }*/

    static Date convertTimeZone(Date d, TimeZone source, TimeZone target) {
        Date.parse('yyyy-MM-dd HH:mm:ss.SSS', d.format('yyyy-MM-dd HH:mm:ss.SSS', target), source)
    }

}
